# 说明

Tvitter 是一个Twitter视频下载器。

**解析器版本**

`python3.8` or above

**依赖**

`requests`

**使用方法**

```sh
tvitter.py https://twitter.com/TsuKiYaZl/status/1305477208203354112
```

*这次重写删除了代理配置文件的支持，但是可以通过添加环境变量实现代理的效果*

```bash
HTTP_PROXY="http://127.0.0.1:8080" HTTPS_PROXY=$HTTP_PROXY tvitter.py https://twitter.com/TsuKiYaZl/status/1305477208203354112
```
