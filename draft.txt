# 已登录与未登录的TwitterAPI会有所区别
# 未登陆
https://api.twitter.com/2/timeline/conversation/1305477208203354112.json?tweet_mode=extended

# 已登陆
https://twitter.com/i/api/2/timeline/conversation/1305477208203354112.json?tweet_mode=extended

API的查询字符串有许多字段，但是只要保留tweet_mode字段即可
API请求头需要用到authorization，这个字段不较难找。但是可以直接复制到headers里发送请求。
x-csrf-token可以省略
如果没有cookie就必须请求一个gust_token这是必须的，
可以通过向https://api.twitter.com/1.1/guest/activate.json发送post请求获得。
请求https://api.twitter.com/1.1/guest/activate.json必须要在请求头中带authorization字段。