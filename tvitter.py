#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import sys

import requests

from typing import Dict, AnyStr, Mapping, Iterable
from functools import cached_property
from abc import ABCMeta, abstractmethod


class GlobalObject(Dict, metaclass=ABCMeta):
    """
    A Container to Save Tweets data.
    """

    @abstractmethod
    def video_url(self) -> AnyStr:
        """
        Parser And Get Video MP4 URL From GlobalObject json.
        :return: Video MP4 URL:String
        """
    @abstractmethod
    def save_video(self) -> None:
        """
        Save the Video Data to Files.
        :return: None
        """


class Tweets(GlobalObject):

    ga_url = 'https://api.twitter.com/1.1/guest/activate.json'
    api_url = 'https://api.twitter.com/2/timeline/conversation/{tweet_id}.json?tweet_mode=extended'
    authorization = 'Bearer AAAAAAAAAAAAAAAAAAAAANRILgAAAAAAnNwIzUejRCOuH5E6I8xnZz4puTs%3D1Zv7ttfk8LF81IUq16cHjhLTvJu4FA33AGWWjCpTnA'

    base_headers = {
        # 'Origin': 'https://twitter.com',
        "Accept-Encoding": "gzip",
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:80.0) Gecko/20100101 Firefox/80.0',
    }

    def __repr__(self) -> str:
        type_name = self.__class__.__name__
        obj_id = id(self)
        data = super().__repr__()

        rt_str = f"<{type_name} - ID: {obj_id}>{data}"

        return rt_str

    def __str__(self) -> str:
        return self.__repr__()

    def __init__(self, data: [AnyStr, Mapping, Iterable]) -> None:

        self._req_sess = requests.Session()
        is_data_str = isinstance(data, str) or isinstance(data, bytes)

        if is_data_str:
            headers = self.base_headers
            headers['authorization'] = self.authorization

            protocol, uri = data.strip().split(':')
            domain, *path = uri.split('/')[2:]
            tweet_id = path[-1].split('?')[0]

            if 'twitter' not in domain:
                raise ValueError('Invalid URL for %s.' % data)

            tweet_data_url = self.api_url.format(tweet_id=tweet_id)

            resp = self._req_sess.post(self.ga_url, headers=headers)
            guest_token = resp.json()['guest_token']

            headers['x-guest-token'] = guest_token

            resp = self._req_sess.get(tweet_data_url, headers=headers)

            tweet_data_json = resp.json()

            super(Tweets, self).__init__(**tweet_data_json['globalObjects']['tweets'][tweet_id])

        elif isinstance(data, Mapping):
            super(Tweets, self).__init__(**data)

        elif isinstance(data, Iterable):
            super(Tweets, self).__init__(*data)

    @cached_property
    def video_url(self) -> AnyStr:
        media = self['extended_entities']['media']
        variants = media[0]['video_info']['variants']
        variants = sorted(variants, key=lambda kv: kv.get('bitrate', 0))
        url = variants[-1]['url']
        return url

    @cached_property
    def _video_data(self) -> bytes:
        headers = self.base_headers
        resp = self._req_sess.get(self.video_url, headers=headers)
        return resp.content

    def save_video(self) -> None:
        tweet_id = self['id_str']
        ext = 'mp4'
        with open(os.path.join(os.path.abspath(os.curdir), '.'.join((tweet_id, ext))), 'wb+') as target:
            target.write(self._video_data)


if __name__ == '__main__':
    url = sys.argv[1]
    twts = Tweets(url)
    twts.save_video()